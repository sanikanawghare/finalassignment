'use strict';
var util = require('util');
var user = require('../../schemas/userDetails');
var product=require('../../schemas/productDetails');
var fs = require('fs');

module.exports = {
    userList:userList,
    deleteUser:deleteUser,
    updateUser:updateUser,
    addProduct:addProduct,
    productList:productList,
    deleteProduct:deleteProduct,
    updateProduct:updateProduct,
    addUserByAdmin:addUserByAdmin,
    deactivateUser:deactivateUser
};
var timeStamp = Date.now();

function addUserByAdmin(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
      console.log(" here",req.body);
  var record = new user();
  record.fullName = req.body.newfname;
  record.email = req.body.newemail;
  record.username = req.body.newuName;
  record.password = req.body.newpsword;
  record.role=req.body.newrole;
  record.isDeactive=req.body.newisDeactive;
  record.save(function (err, res1) {
      console.log(" Resp =", err, res1);
      if(err){
          res.json({
              data: err
          });
      }else{
          res.json({
              message: " Sucessfully added user"
          });
      }
  });
}
function userList(req, res, next) {
    var userArray = [{
      $project: {
        fullName: '$fullName',
        email: '$email',
        username: '$username',
        password: '$password',
        role: '$role',
        isDeactive:'$isDeactive'
      }
    }]
    user.aggregate(userArray, function (err, data) {
      res.json(data);
    })
  }
function deleteUser(req, res) {
    console.log("inside delete",req.swagger.params.id.value)
    user.findByIdAndRemove({ _id: req.swagger.params.id.value }, function (err, data) {
      if (err) throw err;
      console.log("Deleted Successfully");
      data.save();
      res.json(data);
    })
  }
function updateUser(req, res, next) {
    var data = {
        username: req.body.newUsername,
        password: req.body.newPassword
    }
    var condition = { _id: req.swagger.params.id.value }
    console.log(condition, "==", data)
    user.update(condition, { $set: data }, function (err, data1) {
        console.log(" resp====", err, data1);
        if (err) throw err;
        // data1.save();
        console.log("Updated Successfully", data1);
        console.log("Updated data", data1);
        res.json(data1);
    });
    
}

function deactivateUser(req,res){
  var _id=req.swagger.params.id.value;
  var active_status;
  user.findById(_id,function(err,data){
    if(err){
      res.json(err);
    }else if(data){
      var status=data.isDeactive;
      active_status=(status=='Active')?'Deactive':'Active';
      user.findByIdAndUpdate(_id,{$set:{isDeactive:active_status}},function(err,data){
        if(err){
          res.json(err)
        }else{
          data.save();
          console.log("Deactivated",data);
          res.json(data);
        }
      });
    }else{
      res.json("elseeeeeeeeee");
    }
  })
}



function addProduct(req, res, next) {
  var imgOriginalName = req.files.image[0].originalname;
  var path = './ImageUploads/' + timeStamp + "_" + imgOriginalName;
  product.imagePath = path;
  console.log("Image",req.files)
  fs.writeFile(path, (req.files.image[0].buffer), function (err) {
      if (err) throw err;
  })

  console.log("Inside Add Product API")

  var recordDetails = new product();

  recordDetails.productName = req.swagger.params.pname.value;
  // recordDetails.productOwner = req.body.powner;
  recordDetails.productCost = req.swagger.params.pcost.value;
  recordDetails.shippingCost = req.swagger.params.scost.value;
  // recordDetails.shippingAddress = req.body.add;
  // recordDetails.phoneNumber = req.body.pnum;
  recordDetails.status = req.swagger.params.stat.value;
  // recordDetails.date = req.body.date;
  recordDetails.imagePath=path;

  recordDetails.save(function (err, res) {
    console.log(res);
  })
  res.json("added");
}
function productList(req, res, next) {
  //res.render('list', { title: 'Express' });
  var gArray = [{
    $project: {
      productName: '$productName',
      productOwner: '$productOwner',
      productCost: '$productCost',
      shippingCost: '$shippingCost',
      shippingAddress: '$shippingAddress',
      phoneNumber: '$phoneNumber',
      status: '$status',
      date: '$date'
    }
  }]
  product.aggregate(gArray, function (err, data) {
    res.json(data);
  })
}
function deleteProduct(req, res) {
  console.log("inside delete",req.swagger.params.id.value)
  product.findByIdAndRemove({ _id: req.swagger.params.id.value }, function (err, data) {
    if (err) throw err;
    console.log("Deleted Successfully");
    data.save();
    res.json(data);
  })
}
function updateProduct(req, res, next) {
  var data = {
    productName: req.body.newProductName,
    productOwner: req.body.newProductCost
  }
  var condition = { _id: req.swagger.params.id.value }
  console.log(condition, "==", data)
  product.update(condition, { $set: data }, function (err, data1) {
      console.log(" resp====", err, data1);
      if (err) throw err;
      // data1.save();
      console.log("Updated Successfully", data1);
      console.log("Updated data", data1);
      res.json(data1);
  });
  
}
