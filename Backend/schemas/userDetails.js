const mongoose = require("mongoose")

const userDetails = new mongoose.Schema({
    fullName: String,
    email:String,
    username:String,
    password:String,
    role:String,
    isDeactive:String
    });


module.exports= mongoose.model("userDetails", userDetails);
